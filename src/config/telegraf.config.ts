import { TelegrafModuleAsyncOptions } from 'nestjs-telegraf';
import { BOT_NAME } from '../app.constant';
import { BotModule } from '../bot/bot.module';

export const getTelegrafConfig = (): TelegrafModuleAsyncOptions => ({
  botName: BOT_NAME,
  useFactory: () => ({
    token: process.env.BOT_TOKEN,
    middlewares: [],
    include: [BotModule],
  }),
});
