import { ConfigModuleOptions } from '@nestjs/config';

export const getFilePathConfig = (): ConfigModuleOptions => ({
  envFilePath: `.env.${process.env.NODE_ENV}`,
  isGlobal: true,
  
});
