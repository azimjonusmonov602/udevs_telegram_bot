import { Module } from '@nestjs/common';
import { BotModule } from './bot/bot.module';
import { TelegrafModule } from 'nestjs-telegraf';
import { getTelegrafConfig } from './config/telegraf.config';
import { ConfigModule } from '@nestjs/config';
import { getFilePathConfig } from './config/filepath.config';
import { SequelizeModule } from '@nestjs/sequelize';
import { getSequelizeConfig } from './config/sequelize.config';


@Module({
  imports: [
    TelegrafModule.forRootAsync(getTelegrafConfig()),
    ConfigModule.forRoot(getFilePathConfig()),
    SequelizeModule.forRoot(getSequelizeConfig()),

    BotModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
