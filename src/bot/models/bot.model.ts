import { ApiProperty } from '@nestjs/swagger';
import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface BotAttrs {
  user_id: number;
  username: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  status: boolean;
  lang: string;
}

@Table({ tableName: 'bot' })
export class Bot extends Model<Bot, BotAttrs> {
  @ApiProperty({ example: 11, description: `User ID` })
  @Column({
    type: DataType.BIGINT,
    primaryKey: true,
  })
  user_id: number;

  @ApiProperty({ example: `user1`, description: `Username` })
  @Column({
    type: DataType.STRING,
  })
  username: string;

  @ApiProperty({ example: `ali`, description: `First Name of Reader` })
  @Column({
    type: DataType.STRING,
  })
  first_name: string;

  @ApiProperty({ example: `valiyev`, description: `Last Name of Reader` })
  @Column({
    type: DataType.STRING,
  })
  last_name: string;

  @ApiProperty({
    example: `+998901234567`,
    description: `Phone Number of User`,
  })
  @Column({
    type: DataType.STRING,
  })
  phone_number: string;

  @ApiProperty({ example: false, description: `User Status` })
  @Column({
    type: DataType.BOOLEAN,
  })
  status: boolean;

  @Column({
    type: DataType.STRING
  })
  lang: string
}

