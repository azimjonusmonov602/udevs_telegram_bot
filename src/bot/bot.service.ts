import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Bot } from './models/bot.model';
import { InjectBot } from 'nestjs-telegraf';
import { BOT_NAME } from 'src/app.constant';
import { Context, Markup, Telegraf } from 'telegraf';


@Injectable()
export class BotService {
  constructor(
    @InjectModel(Bot) private  botRepo: typeof Bot,
    @InjectBot(BOT_NAME) private readonly bot: Telegraf<Context>,
  ) {
    
  }
  async start(ctx: Context) {
   
    const userId = ctx.from.id;
    const user = await this.botRepo.findOne({
      where: { user_id: userId },
    });

    if (!user) {
      await this.botRepo.create({
        user_id: userId,
        first_name: ctx.from.first_name,
        last_name: ctx.from.last_name,
        username: ctx.from.username,
        lang: ctx.from.language_code
      });
      await ctx.reply(
        `Iltimos, <b>"Telefon raqamni yuborish"</b> tugmasini bosing!`,
        {
          parse_mode: 'HTML',
          ...Markup.keyboard([
            [Markup.button.contactRequest('Telefon raqamni yuborish')],
          ])
            .oneTime()
            .resize(),
        },
      );
    } else if (!user.dataValues.status) {
      await ctx.reply(
        `Iltimos, <b>"Telefon raqamni yuborish"</b> tugmasini bosing!`,
        {
          parse_mode: 'HTML',
          ...Markup.keyboard([
            [Markup.button.contactRequest('Telefon raqamni yuborish')],
          ])
            .oneTime()
            .resize(),
        },
      );
    } else {
      await this.bot.telegram.sendChatAction(userId, 'typing');
      await ctx.reply(
        "Botga xush kelibsiz",
        {
          parse_mode: 'HTML',
          ...Markup.removeKeyboard(),
        },
      );
    }
  }

  async contact(ctx: Context) {
    if ('contact' in ctx.message) {
      const userId = ctx.from.id;
      const user = await this.botRepo.findOne({
        where: { user_id: userId },
      });

      if (!user) {
        await ctx.reply(`Iltimos, <b>Start</b> tugmasini bosing!`, {
          parse_mode: 'HTML',
          ...Markup.keyboard([[Markup.button.contactRequest('/Start')]])
            .oneTime()
            .resize(),
        });
      } else if (ctx.message.contact.user_id !== userId) {
        await ctx.reply(`Iltimos, o'zingizning telefon raqamingizni yuboring!`, {
          parse_mode: 'HTML',
          ...Markup.removeKeyboard(),
        });
      } else {
        let phone: string;
        ctx.message.contact.phone_number[0] === '+'
          ? (phone = ctx.message.contact.phone_number)
          : (phone = '+' + ctx.message.contact.phone_number);

        await this.botRepo.update(
          { phone_number: phone, status: true },
          { where: { user_id: userId }, returning: true },
        );

        await ctx.reply(`Tabriklaymiz, ro'yxatdan o'tdingiz!`, {
          parse_mode: 'HTML',
          ...Markup.removeKeyboard(),
        });
      }
    }
  }
  async getMyData(ctx: Context){
   const  user_id = ctx.from.id
   const user = await this.botRepo.findOne({where: {user_id}})
   if(!user){
    await ctx.reply(`User not found!`, {
      parse_mode: 'HTML',
      ...Markup.removeKeyboard()
    })
   }
   await ctx.reply(`<b>Ismingiz</b>: ${user.first_name}\nlanguage: ${user.lang}`, {
    parse_mode: 'HTML',
    ...Markup.removeKeyboard()
   })
  }
}
